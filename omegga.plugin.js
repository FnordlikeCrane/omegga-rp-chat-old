// RP-Chat
/*--------------------------------------------------------------------------*/
// CONSTS

const fs = require('fs');
const path = require('path');

const NAMES_FILE = path.join(__dirname, 'aliases.json');

const {
  chat: { sanitize },
} = OMEGGA_UTIL;

const colorKey = (color, str) => `"<color=\\"${color}\\">${sanitize(str + "")}</>"`.trim();
const cyan = (str) => colorKey("2ee7ea", str);
const yellow = (str) => colorKey("ffff00", str);
const red = (str) => colorKey("ff5555", str);
const green = (str) => colorKey("55ff55", str);
const grey = (str) => colorKey("aaaaaa", str);
const orange = (str) => colorKey("ff8000", str);
const periwinkle = (str) => colorKey("ccccff", str);

const nameText = (who, alias) => {
  if ( alias !== undefined ) {
    return `"<b><color=\\"${Omegga.getPlayer(who).getNameColor()}\\">${alias}</></>"`.trim();
  } else {
    return `"<b><color=\\"${Omegga.getPlayer(who).getNameColor()}\\">${who}</></>"`.trim();
  }
}

const distance = (pos1, pos2) => {
  const x1 = Math.max(pos1[0], pos2[0]);
  const x2 = Math.min(pos1[0], pos2[0]);

  const y1 = Math.max(pos1[1], pos2[1]);
  const y2 = Math.min(pos1[1], pos2[1]);

  const z1 = Math.max(pos1[2], pos2[2]);
  const z2 = Math.min(pos1[2], pos2[2]);

  const a = (x1-x2)*(x1-x2);
  const b = (y1-y2)*(y1-y2);
  const c = (z1-z2)*(z1-z2);

  return Math.sqrt(a+b+c);
};

const stringFromArr= (...arr) => {
  let str = '';
  for ( const word of arr ) {
    str += word + ' ';
  }
  return str.trim();
}

/*--------------------------------------------------------------------------*/
// RP-Chat Omegga Export

module.exports = class RP_Chat {
  constructor(omegga, config, store) {
    // OMEGGA stuff
    this.omegga = omegga;
    this.config = config;
    this.store = store;

    // For getting player positions
    this.promise;
    this.getting = false;
    this.lastPosTime = 0;
    this.lastPos = [];

    // For nicknames
    this.player_nicknames = {};
  }

  //==========================================================================
  // Get all player positions async safe. This without a doubt ripped from 
  // the chat garbler.
  async getPositions() {
    if (this.getting)
      return await this.promise;
    if (this.lastPosTime + 200 > Date.now())
      return this.lastPos;
    this.getting = true;
    let res;
    this.promise = new Promise(resolve => {res = resolve;});
    const pos = await this.omegga.getAllPlayerPositions();
    this.lastPosTime = Date.now();
    this.getting = false;
    res(pos);
    return pos;
  }

  //==========================================================================
  // Broadcasting shortcut

  async messageTo(msg, whom) {
    if ( whom === undefined || whom === '' || whom === null ) {
      Omegga.broadcast(msg);
    } else {
      Omegga.whisper(whom, msg);
    }
  }


  //==========================================================================
  // Out Of Context: message everyone regardless of status

  async oocCmd(who, ...words) {
    if ( words.length === 0 ) {
      this.messageTo(red("No message provided."), who);
      return;
    }

    let msg = '';
    for ( const word of words ) {
      msg += word + ' ';
    }

    this.messageTo(`${nameText(who)}${grey("[OOC]")}: ${msg}`);
  }


  //==========================================================================
  // Get any aliases of a player
  getCurrentName(who) {
    if ( this.player_nicknames[who] !== undefined ) {
      return this.player_nicknames[who];
    } else {
      return who;
    }
  }


  //==========================================================================
  // Check the distance between one source point and a list of points,
  // returning a list of positions that are in range

  checkDistances(who, source_pos, positions, range) {
    let in_range = [];
    const all_players = this.omegga.players;

    for ( const player of all_players ) {
      const to_pos = (positions.find(p => p.player.name === player.name) || {pos: [0, 0, Infinity]}).pos;
      const dist = distance(source_pos, to_pos)
      if ( dist <= range ) {
        const to_is_dead = (positions.find(p => p.player.name === player.name) || {isDead: true}).isDead;
        if ( to_is_dead === false ) {
          in_range.push({player: player, dist: dist});
        }
      } 
    }

    return in_range;
  }

  // Distanced chat: A function to handle generic non-targetted 
  // communication that's limited by distance to save on lines of code.
  
  //==========================================================================
  // Generic Distanced Based Chat

  async distancedChat(who, kind, ...words) {
    if ( words.length === 0 ) {
      this.messageTo(red("No message provided."), who);
      return 1;
    }
    // Check to see if the sender and or person to whisper to is dead.
    let positions = []
    try {
      positions = await this.getPositions();
    } catch(e) {
      console.error(e);
    }

    const source_dead = (positions.find(p => p.player.name === who) || {isDead: true}).isDead;
    if ( source_dead ) {
      this.messageTo(red("You may not use this command while dead."), who);
      return 2;
    }

    
    let range;
    if ( kind === 'yell' ) {
      range = this.config['Yell distance'];
    } else if ( kind === 'quiet' ) {
      range = this.config['Quiet distance'];
    } else if ( kind === 'standard' ) {
      range = this.config['Standard speaking distance'];
    }
    
    const source_pos = (positions.find(p => p.player.name === who) || {pos: [0, 0, Infinity]}).pos;
    const in_range = this.checkDistances(who, source_pos, positions, range);
    
    // Assemble a message from the array of words passed in
    let msg = stringFromArr(...words);
    
    // Get any player nicknames
    let presenting = this.getCurrentName(who);
  
    // Assemble a chat prefix
    let prefix;
    if ( kind === 'yell' ) {
      prefix = `${nameText(who, presenting)}${orange(`[Yell]`)}`
    } else if ( kind === 'quiet' ) {
      prefix = `${nameText(who, presenting)}${periwinkle(`[Quiet]`)}`
    } else if ( kind === 'standard' ) {
      prefix = `${nameText(who, presenting)}`;
    }

    for ( const item of in_range ) {
      if ( item.dist === 0 && kind === 'standard' ) {
        if ( this.getCurrentName(who) !== who ) this.messageTo(grey(`(as ${this.getCurrentName(who)})`), who);
        continue;
      }
      this.messageTo(`${prefix}: ${msg}`, item.player);
    }

    // For moderation purposes
    if ( kind !== 'standard' ) {
      console.log(`${who} [${kind}]: ${msg}`);
    }
    return 0;
  }


  //==========================================================================
  // Yelling: message to those further away

  async yellCmd(who, ...words) {
    const result = await this.distancedChat(who, 'yell', ...words);
  
  }

  
  //==========================================================================
  // Quiet voice: message to those in a very close radius

  async quietCmd(who, ...words) {
    const result = await this.distancedChat(who, 'quiet', ...words);
  }


  //==========================================================================
  // Regular speaking voice, so regular distance require to hear the message

  async speakCmd(who, ...words) {
    const result = this.distancedChat(who, 'standard', ...words);
  }

  
  //==========================================================================
  // Whisper: Tell only 1 target the message, but must be within range

  async whisperCmd(who, whispering, ...words) {
    // Validate player name
    if ( whispering === undefined ) {
      this.messageTo(red("The player you're searching for cannot be found"), who)
      return;
    }
    const whisper_to = this.omegga.findPlayerByName(whispering);
    if ( whisper_to === undefined ) {
      this.messageTo(`${red("The player you're searching for cannot be found")}: ${yellow(whispering)}`, who)
      return;
    } 
    // Validate message
    else if ( words.length === 0 ) {
      this.messageTo(red("No message provided."), who);
      return;
    }

    // Check to see if the sender and or person to whisper to is dead.
    let positions = []
    try {
      positions = await this.getPositions();
    } catch(e) {
      console.error(e);
    }
    const from_is_dead =  (positions.find(p => p.player.name === who) || {isDead: true}).isDead;
    const to_is_dead = (positions.find(p => p.player.name === whisper_to.name) || {isDead: true}).isDead;

    if ( from_is_dead && !this.omegga.getPlayer(who).isHost()) {
      this.messageTo(red("You must be alive to use this command."), who);
      return;
    } else if (to_is_dead && !this.omegga.getPlayer(who).isHost()) {
      this.messageTo(red("The message could not be received!"), who)
      this.messageTo(`${yellow(whisper_to.name)}is either dead or not within range.`, who);
      return;
    }

    // Check the distance between the players
    const from_pos = (positions.find(p => p.player.name === who) || {pos: [0, 0, Infinity]}).pos;
    const to_pos = (positions.find(p => p.player.name === whisper_to.name) || {pos: [0, 0, Infinity]}).pos;;
    const dist = distance(from_pos, to_pos);

    if ( dist > this.config['Whisper distance']) {
      this.messageTo(red("The message could not be received!"), who)
      this.messageTo(`${yellow(whisper_to.name)}is either dead or not within range.`, who);
      return;
    }

    const from_alias = this.getCurrentName(who);
    const to_alias = this.getCurrentName(whisper_to.name);

    // Assemble the message into a string from the arguments passed through
    const msg = stringFromArr(...words)

    if ( whisper_to.name !== who ) {
      this.messageTo(`${nameText(who, from_alias)}${cyan(`[Whisper » ${to_alias}]`)}: ${msg}`, who);
    }
    this.messageTo(`${nameText(who, from_alias)}${cyan(`[Whisper » ${to_alias}]`)}: ${msg}`, whisper_to);
    
    console.log(`${who} [Whisper » ${to_alias}]: ${msg}`);
  }


  //==========================================================================
  // Tell: DMing others info. Is not dependent on distance

  async tellCmd(who, telling, ...words) {
    // Validate player name
    if ( telling === undefined ) {
      this.messageTo(red("The player you're searching for cannot be found"), who)
      return;
    }
    const telling_to = this.omegga.findPlayerByName(telling);
    if ( telling_to === undefined ) {
      this.messageTo(`${red("The player you're searching for cannot be found")}: ${yellow(telling)}`, who)
      return;
    }

    // Check to see if the sender is dead
    let positions = []
    try {
      positions = await this.getPositions();
    } catch(e) {
      console.error(e);
    }
    const from_is_dead =  (positions.find(p => p.player.name === who) || {isDead: true}).isDead;
    if ( from_is_dead && this.config['Alive to tell'] && !this.omegga.getPlayer(who).isHost()) {
      this.messageTo(red("You must be alive to use this command."), who);
      return;
    }

    // Make sure there's a message available
    if ( words.length === 0 ) {
      this.messageTo(red("No message provided."), who);
      return;
    }

    const from_alias = this.getCurrentName(who);
    const to_alias = this.getCurrentName(telling_to.name);

    const msg = stringFromArr(...words);

    if ( telling_to.name !== who ) {
      this.messageTo(`${nameText(who, from_alias)}${green(`[Tell » ${to_alias}]`)}: ${msg}`, who);
    }
    this.messageTo(`${nameText(who, from_alias)}${green(`[Tell » ${to_alias}]`)}: ${msg}`, telling_to);
    console.log(`${who} [Tell » ${to_alias}]: ${msg}`);
  }


  //==========================================================================
  // Rolling dice
  
  async rollCmd(who, type, ...args) {
    if ( this.config['Host only dice'] && !this.omegga.getPlayer(who).isHost() ) {
      this.messageTo(`${red("Rolling dice is currently host-only.")}`, who);
      return;
    } else if ( this.config['Enable dice'] !== true && !this.omegga.getPlayer(who).isHost() ) {
      this.messageTo(`${red("Rolling dice is currently disabled.")}`, who);
      return;
    }

    if ( this.config['Banned from dice'].includes(this.omegga.getPlayer(who)) ) {
      this.messageTo(red("You have been bannd from rolling dice."), who);
      return;
    }

    let arg;
    for (let curr = 0; curr < args.length; curr++) {
      arg = args[curr];
      let i;
      let d,
        num,
        face = null;

      const len = arg.length;
      if ( len === 0 ) continue;

      if ( arg.includes('d') === false ) {
        this.messageTo(red("You have not entered any dice to roll."), who);
        continue;
      }
      // Parse text to find what dice are being rolled
      for (i = 0; i < len; i++) {
        d = arg.substring(i, i + 1);
        if (d === "d") {
          face = arg.substring(i + 1, len);
          num = arg.substring(0, i);
          break;
        } else {
          num = arg.substring(0, i + 1);
        }
      }

      if ( typeof num !== "number" && isFinite(num) && num.length === 0) {
        num = 1;
      } else {
        num = Math.ceil(num);
        if (num > this.config["Max dice per roll"]) {
          this.messageTo(red(`Limit number of rolls to ${this.config["Max dice per roll"]}.`), who);
          continue;
        } else if ( num < 1 ) {
          this.messageTo(red("What's the point in rolling nothing?"), who);
          continue;
        }
      }
      if (face === null || face < 2) {
        this.messageTo(red("Please enter a dice face to roll."), who);
        continue;
      } else if (face > this.config["Largest face per die"]) {
        this.messageTo(`Limit dice face size to ${this.config["Largest face per die"]}.`, who);
        return;
      } else {
        face = Math.floor(face);
      }

      let all_rolls = "";
      let total = 0;

      if ( num > 1 ) {
        this.messageTo("<b>Rolled</b>:", type);
      }
      for (i = 0; i < num; i++) {
        let roll = Math.floor(Math.random() * face + 1);
        const tmp = roll + "";
        if (tmp === "NaN") {
          this.messageTo(`${red("Cannot roll that.")}`, who);
        } else {
          let display = roll + "";
          if (roll === 1) {
            display = red(display);
          } else if (roll + "" === face + "") {
            display = green(display);
          }
          if (num > 1) {
            if (all_rolls === "") {
              all_rolls = '';
            } else if ( all_rolls.length > 200 ) {
              this.messageTo(all_rolls, type);
              all_rolls = '';
            }
            all_rolls += `${grey("[")}${display} / ${yellow(face + "")}${grey("]")}`;
            total += roll;
            if (i+1 < num) {
              all_rolls += " + ";
            }
          } else {
            this.messageTo(`<b>Rolled</b>: ${display} / ${yellow(face + "")}`, type);
          }
        }
      }

      if (num > 1) {
        all_rolls += `= ${yellow(total + "")}`;
        this.messageTo(all_rolls, type);
      }
    }
  }

  //==========================================================================
  // ALIAS COMMANDS

  loadAliases() {
    fs.readFile(NAMES_FILE, 'utf8', (err, data) => {
      if ( err ) {
        console.error(err);
      } else {
        let found;
        try {
          found = JSON.parse(data);
        } catch(e) {
          console.error(e);
        }
        this.player_nicknames = found;
        console.log("Aliases loaded.");
      }
    });  
  }

  saveAliases() {
    fs.writeFile(NAMES_FILE, JSON.stringify(this.player_nicknames), 'utf8', (err) => {
      if ( err ) {
        console.error(err);
      } else {
        console.log("Aliases saved.");
      }
    });
  }

  async aliasCmd(who, ...nickname_str) {
    if ( this.config["Banned from aliasing"].includes(who) ) {
      this.messageTo(red("You are banned from using aliases"), who);
      return;
    }
    
    if ( nickname_str.length === 0 ) {
      this.messageTo(`Your current alias is ${yellow(this.getCurrentName(who))}`, who);
      return;
    }
    const nickname = stringFromArr(...nickname_str);
    if ( this.getCurrentName(who) === nickname ) {
      this.player_nicknames[who] = undefined;
      this.messageTo("You have removed your alias", who);
      return;
    }
    
    const find_existing = (this.omegga.players.find(p => p.name === nickname) || {name: undefined}).name;
    if ( find_existing === who ) {
      this.player_nicknames[who] = undefined;
      this.messageTo("You have removed your alias", who);
      return;
    }
    else if ( find_existing !== undefined ) {
      this.messageTo(red("You may not use an alias of an existing player."), who);
      return;
    }

    this.player_nicknames[who] = nickname;
    this.messageTo(`You have changed your alias to ${yellow(nickname)}`, who);
    this.saveAliases();
  }

  async aliasLookup(who, ...lookup_arr) {
    if ( lookup_arr.length !== 0 ) {
      const lookup_str = stringFromArr(...lookup_arr);
      const lookup = this.omegga.findPlayerByName(lookup_str);

      if ( lookup === undefined ) {
        this.messageTo(`No player found with the name: ${red(lookup_str)}`, who);
        return;
      } else {

        const found_alias = this.player_nicknames[lookup.name];
        if ( found_alias === undefined ) {
          this.messageTo(`No player alias could be found for ${yellow(lookup.name)}`, who);
        } else {
          this.messageTo(`Alias for player ${nameText(lookup.name)}: ${yellow(found_alias)}`, who);
        }
      }
    }
  }

  async aliasRemove(who, ...remove_from_arr) {
    
    if ( remove_from_arr.length !== 0 && this.omegga.getPlayer(who).isHost() ) {
      const remove_from_str = stringFromArr(...remove_from_arr);
      const lookup = this.omegga.findPlayerByName(remove_from_str);
      if ( lookup !== undefined ) {
        const name = lookup.name;
        this.player_nicknames[name] = undefined;
        this.messageTo(`${yellow(name)}'s alias has been removed.`, who);
        this.messageTo(red("Your alias has been removed manually by an admin."), name);
        this.saveAliases();
      } else {
        this.messageTo(`Could not find player with name ${red(remove_from_str)}`, who);
      }
    } else {
      this.player_nicknames[who] = undefined;
      this.messageTo("You have removed your alias", who);
      this.saveAliases();
    }
  }

  init() {
    /* 
     * COMMANDS 
     */
    //------------------------------------------------------------------------
    // Dice
    
    // Public dice rolling
    Omegga.on("chatcmd:roll", async (name, ...args) => {
      try {
        await this.rollCmd(name, null, ...args);
      } catch(e) {
        console.error(e);
      }
    });
    // Private dice rolling
    Omegga.on("cmd:roll", async (name, ...args) => {
      try {
        await this.rollCmd(name, name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Out of Context
    Omegga.on('cmd:ooc', async (name, ...args) => {
      try {
        await this.oocCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });
    
    // OOC alias
    Omegga.on('cmd:/', async (name, ...args) => {
      try {
        await this.oocCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Yelling
    
    Omegga.on('cmd:yell', async (name, ...args) => {
      try {
        await this.yellCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    // Yell alias
    Omegga.on('cmd:y', async (name, ...args) => {
      try {
        await this.yellCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Whispering

    Omegga.on('cmd:whisper', async (name, to, ...args) => {
      try {
        await this.whisperCmd(name, to, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    // Whisper alias
    Omegga.on('cmd:w', async (name, to, ...args) => {
      try {
        await this.whisperCmd(name, to, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Quiet speaking

    Omegga.on('cmd:quiet', async (name, ...args) => {
      try {
        await this.quietCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });    

    // Quiet alias
    Omegga.on('cmd:q', async (name, ...args) => {
      try {
        await this.quietCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Direct messaging

    Omegga.on('cmd:tell', async (name, telling, ...args) => {
      try {
        await this.tellCmd(name, telling, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    // Tell alias
    Omegga.on('cmd:t', async (name, telling, ...args) => {
      try {
        await this.tellCmd(name, telling, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // Nickname Commands

    Omegga.on('cmd:alias', async (name, ...nickname) => {
      try {
        await this.aliasCmd(name, ...nickname);
      } catch(e) {
        console.error(e);
      }
    });

    Omegga.on('cmd:alias:remove', async (name, ...who) => {
      try {
        await this.aliasRemove(name, ...who);
      } catch(e) {
        console.error(e);
      }
    });
    Omegga.on('cmd:alias:rm', async (name, ...who)=> {
      try {
        await this.aliasRemove(name, ...who);
      } catch(e) {
        console.error(e);
      }
    });

    Omegga.on('cmd:alias:lookup', async (name, ...who) => {
      try {
        await this.aliasLookup(name, ...who);
      } catch(e) {
        console.error(e);
      }
    });

    //------------------------------------------------------------------------
    // MISC

    // Basic chatting.
    Omegga.on('chat', async (name, ...args) => {
      try {  
        await this.speakCmd(name, ...args);
      } catch(e) {
        console.error(e);
      }
    });

    this.loadAliases();

    return {
      registeredCommands: [
        't', 'tell', 
        'w', 'whisper',
        'q', 'quiet',
        '/', 'ooc',
        'y', 'yell',
        'roll',
        'alias', 'alias:rm', 'alias:remove', 'alias:lookup'
      ]
    };
  }

  stop() {
    this.saveAliases();
  }
};
